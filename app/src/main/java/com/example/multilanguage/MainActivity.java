package com.example.multilanguage;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocal();
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChooseLanguageDialog();
            }
        });
    }

    private void showChooseLanguageDialog() {
        String language[] = {"English", "Khmer", "Thai"};
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this)
                .setTitle("Choose a Language")
                .setSingleChoiceItems(language, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if ( which == 0){
                            setLocalLanguage("en");
                            recreate();
                            dialog.dismiss();
                        }
                        if (which == 1){
                            setLocalLanguage("km");
                            recreate();
                            dialog.dismiss();
                        }
                        if (which == 2){
                            setLocalLanguage("th");
                            recreate();
                            dialog.dismiss();
                        }

                    }
                });

        AlertDialog alert = alertDialog.create();
        alert.show();

    }

    public void setLocalLanguage(String language){

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();

        editor.putString("My_lang", language);
        editor.apply();

    }

    public void loadLocal(){
        SharedPreferences preferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String lang = preferences.getString("My_lang","");
        setLocalLanguage(lang);
    }
}
